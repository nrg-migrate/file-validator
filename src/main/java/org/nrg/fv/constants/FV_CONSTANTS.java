package org.nrg.fv.constants;

import java.io.File;

public class FV_CONSTANTS {
	public static String HOST="test";
	public static String USER="test";
	
	public static String LOG_DIR="." + File.separator + "logs";
	public static String CURRENT="." + File.separator + "current" + File.separator + "last.csv";
	
	public static String ADMIN_EMAIL="test@test.edu";
	public static String SMTP_SERVER="test.test.edu";
	
	public static Boolean LIST_ADDED_FILES=Boolean.FALSE;
	
	public static final Boolean DEBUG=false;
}
