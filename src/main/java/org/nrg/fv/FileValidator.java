package org.nrg.fv;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.HtmlEmail;
import org.nrg.fv.actions.BuildSummary;
import org.nrg.fv.actions.ExperimentListRetriever;
import org.nrg.fv.actions.FileSystemTracker;
import org.nrg.fv.actions.LastListManager;
import org.nrg.fv.actions.SessionRetriever;
import org.nrg.fv.constants.FV_CONSTANTS;
import org.nrg.fv.entry.DiffEntry;
import org.nrg.fv.entry.EntryComparator;
import org.nrg.fv.entry.ResourceEntry;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.xml.sax.SAXException;

public class FileValidator {
	public static int limit=-1;
	
	public static void printHelp(){
		System.out.println("Parameters:");
		System.out.println("0: xnat user:password");
		System.out.println("1: xnat site url");
		System.out.println("2: listAddedFiles");
		System.out.println("3: email address");
		System.out.println("4: smtp server");
		System.out.println("5: levels to check (1111=ALL,0000=none)");
		System.out.println("    index summary:");
		System.out.println("      0:resources");
		System.out.println("      1:scans");
		System.out.println("      2:reconstructions");
		System.out.println("      3:assessments");
		System.out.println("6: session limit");
		System.exit(1);
	}
	
	public static void main(String[] args){
		if(args.length!=6 && args.length!=7){
			printHelp();
		}
		
		FV_CONSTANTS.USER=args[0];
		FV_CONSTANTS.HOST=args[1];
		FV_CONSTANTS.LIST_ADDED_FILES=Boolean.valueOf(args[2]);
		FV_CONSTANTS.ADMIN_EMAIL=args[3];
		FV_CONSTANTS.SMTP_SERVER=args[4];
		
		String levels=args[5];
		if(levels.length()!=4){
			printHelp();
			System.exit(1);
		}else{
			if(levels.charAt(0)=='0'){
				FileSystemTracker.TRACK_RESOURCES=false;
			}else{
				FileSystemTracker.TRACK_RESOURCES=true;
			}
			
			if(levels.charAt(1)=='0'){
				FileSystemTracker.TRACK_SCANS=false;
			}else{
				FileSystemTracker.TRACK_SCANS=true;
			}
			
			if(levels.charAt(2)=='0'){
				FileSystemTracker.TRACK_RECONS=false;
			}else{
				FileSystemTracker.TRACK_RECONS=true;
			}
			
			if(levels.charAt(3)=='0'){
				FileSystemTracker.TRACK_ASSESS=false;
			}else{
				FileSystemTracker.TRACK_ASSESS=true;
			}
		}
		
		if(args.length==7)
		limit=Integer.parseInt(args[6]);			
		
		FileValidator fv=new FileValidator();
		fv.execute();
	}
	
	public void execute(){
		final ExperimentListRetriever etr = new ExperimentListRetriever();
		final SessionRetriever sr = new SessionRetriever();
		final FileSystemTracker fst=new FileSystemTracker();
		
		final BuildSummary bs=new BuildSummary();
		final LastListManager rll=new LastListManager();
		
		
		try {
			final List<String> msgs=new ArrayList<String>();
			final List<String> expts=etr.retrieveCurrentList();
			final List<ResourceEntry> resources=new ArrayList<ResourceEntry>();
			
			int c=0;
			for(final String id:expts){
				System.out.print("\nReviewing " + c +" of " + expts.size() + " ("+ id + ")");
				try {
					final long starttime = Calendar.getInstance().getTimeInMillis();
					final XnatImagesessiondataBean session=sr.retrieveSession(id);
					
					System.out.print("... " +(Calendar.getInstance().getTimeInMillis()-starttime) + "ms");
					
					for(final ResourceEntry re:fst.trackFiles(session))
					{
						re.EXPT=session.getId();
						re.LABEL=session.getLabel();
						re.PROJECT=session.getProject();
						resources.add(re);
					}
					
					System.out.println("... " +(Calendar.getInstance().getTimeInMillis()-starttime) + "ms");
					
					if(++c==limit){
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
					msgs.add("Error processing " + id + ". "+ e.getMessage());
				}
			}

			Collections.sort(resources,new EntryComparator());
			
			final List<ResourceEntry> last=rll.retrieve();
			
			final List<List<DiffEntry>> diffs=bs.compare(last, resources);
			
			final String msg=writeMessage(diffs, msgs);
			
			rll.write(resources);

			rll.writeOutput(msg);
			this.sendMessage(diffs.get(4).size()+diffs.get(2).size(), msg);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String writeMessage(List<List<DiffEntry>> diffs,List<String> msgs){
		//0:delete
		//1:absent
		//2:modified
		//3:new
		//4:modified ABSENT
		final StringBuffer sb= new StringBuffer();

		sb.append("Summary From: " + FV_CONSTANTS.HOST);
		
		final int PROJECT_WIDTH = 30;
		final int EXPT_WIDTH = 30;
		final int LEVEL_WIDTH = 14;
		final int LEVEL_ID_WIDTH = 40;
		final int LABEL_WIDTH = 10;
		if(diffs.get(4).size()>0){
			sb.append("\r\n\r\n<br><br>Resources which are now absent, but were not deleted from the XML Metadata.");
			for(final DiffEntry de:diffs.get(4)){
				sb.append("\r\n<br>").append(StringUtils.rightPad(de.getProject(), PROJECT_WIDTH))
				.append(StringUtils.rightPad(de.getExpt(), EXPT_WIDTH))
				.append(StringUtils.rightPad(de.getLevel(), LEVEL_WIDTH))
				.append(StringUtils.rightPad(de.getLevel_id(), LEVEL_ID_WIDTH))
				.append(StringUtils.rightPad(de.getId(), LABEL_WIDTH));
			}
		}
		
		if(diffs.get(2).size()>0){
			sb.append("\r\n\r\n<br><br>Resources which have been modified.");
			for(final DiffEntry de:diffs.get(2)){
				sb.append("\r\n<br>").append(StringUtils.rightPad(de.getProject(), PROJECT_WIDTH))
				.append(StringUtils.rightPad(de.getExpt(), EXPT_WIDTH))
				.append(StringUtils.rightPad(de.getLevel(), LEVEL_WIDTH))
				.append(StringUtils.rightPad(de.getLevel_id(), LEVEL_ID_WIDTH))
				.append(StringUtils.rightPad(de.getId(), LABEL_WIDTH))
				.append(StringUtils.rightPad(de.oldCount.toString(), 4))
				.append(StringUtils.rightPad(String.format("%1$tm-%1$td-%1$ty %1$tH:%1$tM:%1$tS",new Date(de.oldModTime)), 20))
				.append(StringUtils.rightPad(de.newCount.toString(), 4))
				.append(StringUtils.rightPad(String.format("%1$tm-%1$td-%1$ty %1$tH:%1$tM:%1$tS",new Date(de.newModTime)), 20));
			}
		}

		if(diffs.get(0).size()>0){
			sb.append("\r\n\r\n<br><br>Resources which have been deleted from the XML Metadata (may or may not be present on the file-system).");
			for(final DiffEntry de:diffs.get(0)){
				sb.append("\r\n<br>").append(StringUtils.rightPad(de.getProject(), PROJECT_WIDTH))
				.append(StringUtils.rightPad(de.getExpt(), EXPT_WIDTH))
				.append(StringUtils.rightPad(de.getLevel(), LEVEL_WIDTH))
				.append(StringUtils.rightPad(de.getLevel_id(), LEVEL_ID_WIDTH))
				.append(StringUtils.rightPad(de.getId(), LABEL_WIDTH));
			}
		}

		if(diffs.get(1).size()>0){
			sb.append("\r\n\r\n<br><br>Resources which lacked and continue to lack corresponding files on the file system.");
			for(final DiffEntry de:diffs.get(1)){
				if(!de.getId().equals("SNAPSHOTS")){
					sb.append("\r\n<br>").append(StringUtils.rightPad(de.getProject(), PROJECT_WIDTH))
					.append(StringUtils.rightPad(de.getExpt(), EXPT_WIDTH))
					.append(StringUtils.rightPad(de.getLabel(), EXPT_WIDTH))
					.append(StringUtils.rightPad(de.getLevel(), LEVEL_WIDTH))
					.append(StringUtils.rightPad(de.getLevel_id(), LEVEL_ID_WIDTH))
					.append(StringUtils.rightPad(de.getId(), LABEL_WIDTH));
				}
			}
		}

		if(FV_CONSTANTS.LIST_ADDED_FILES && diffs.get(3).size()>0){
			sb.append("\r\n\r\n<br><br>Resources which have been added for new or existing.");
			for(final DiffEntry de:diffs.get(3)){
				sb.append("\r\n<br>").append(StringUtils.rightPad(de.getProject(), PROJECT_WIDTH))
				.append(StringUtils.rightPad(de.getExpt(), EXPT_WIDTH))
				.append(StringUtils.rightPad(de.getLevel(), LEVEL_WIDTH))
				.append(StringUtils.rightPad(de.getLevel_id(), LEVEL_ID_WIDTH))
				.append(StringUtils.rightPad(de.getId(), LABEL_WIDTH));
			}
		}
		
		if(msgs.size()>0){
			sb.append("\r\n\r\n<br><br>Errors");
			for(final String s:msgs){
				sb.append("\r\n<br>").append(s);
			}
		}
		
		return sb.toString();
	}


	public void sendMessage(int deletes,String msg){
		final HtmlEmail sm = new HtmlEmail();
		try {
			sm.setHostName(FV_CONSTANTS.SMTP_SERVER);
			sm.setFrom(FV_CONSTANTS.ADMIN_EMAIL);
			
			sm.addTo(FV_CONSTANTS.ADMIN_EMAIL);
	        		
			if(deletes==0)
				sm.setSubject("FILE REVIEW: NO MODIFICATIONS");
			else if(deletes==-1){
				sm.setSubject("ERROR: File validator");
			}else
				sm.setSubject("FILE REVIEW: " +deletes +" Resources removed or modified from file system.");
			
			sm.setHtmlMsg(msg);
			sm.setTextMsg(msg);
            sm.send();
		    
        } catch (MessagingException e) {
		    e.printStackTrace();
        }
	}
}
