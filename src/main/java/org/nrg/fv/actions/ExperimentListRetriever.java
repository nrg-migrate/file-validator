package org.nrg.fv.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nrg.fv.constants.FV_CONSTANTS;
import org.nrg.fv.utils.Utils;

public class ExperimentListRetriever {
	public static String EXPT="/REST/experiments?format=csv&xsiType=xnat:imageSessionData&columns=ID";
	
	public List<String> retrieveCurrentList() throws MalformedURLException,IOException,Exception{
		List<String> expts=new ArrayList<String>();

		BufferedReader br = null;
		String thisLine = null;
		
		URL site= new URL(FV_CONSTANTS.HOST + EXPT);
		
		URLConnection uc = null;
		
		uc = site.openConnection();

		uc.setRequestProperty("Authorization", "Basic "
				+ com.Ostermiller.util.Base64.encode(FV_CONSTANTS.USER));
		
		br = new BufferedReader(new InputStreamReader(uc.getInputStream()));

		int index=0;
		
		String header=null;
		while ((thisLine = br.readLine()) != null) {
			if(header==null){
				header=thisLine;
				
				boolean matched=false;
				for(String h:Utils.CommaDelimitedStringToArrayList(header)){
					if(StringUtils.remove(h, '"').equals("ID")){
						matched=true;
						break;
					}else if(StringUtils.remove(h, '"').equals("xnat_imagesessiondata0")){
						//added to temporarily get around bug until CNDA is updated.
						matched=true;
						break;
					}else{
						index++;
					}
				}
				
				if(!matched){
					throw new Exception("Unable to find ID field");
				}
			}else {
				expts.add(StringUtils.remove(Utils.CommaDelimitedStringToArrayList(thisLine).get(index), '"'));
			}
		}
		
		return expts;
	}
	
	public static void main(String[] args) throws Exception {
		ExperimentListRetriever elr=new ExperimentListRetriever();
		try {
			List<String> expts=elr.retrieveCurrentList();
			System.out.println(expts);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	
}
