package org.nrg.fv.actions;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.nrg.fv.constants.FV_CONSTANTS;
import org.nrg.fv.entry.EntryComparator;
import org.nrg.fv.entry.ResourceEntry;
import org.nrg.fv.utils.Utils;

public class LastListManager {
	
	public List<ResourceEntry> retrieve() throws IOException{
		ArrayList<ResourceEntry> last= new ArrayList<ResourceEntry>();
		
		File current= new File(FV_CONSTANTS.CURRENT);
		
		if(current.exists()){
			DataInputStream dis=null;
			try {
				FileInputStream in = new FileInputStream(current);
				dis = new DataInputStream(in);
				while (dis.available() !=0)
				{
					last.add(ResourceEntry.parse(dis.readLine()));
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				dis.close();
			}
		}
		
		EntryComparator ec = new EntryComparator();
		Collections.sort(last,ec);
		return last;
	}
	
	public void writeOutput(String msg) throws IOException{
		String dir=(Calendar.getInstance().getTime().getMonth()+1) + "_" + Calendar.getInstance().getTime().getDate() + "_" + (Calendar.getInstance().getTime().getYear()+1900) ;
		
		File dest = new File("." + File.separator + dir+ File.separator + dir +"_out.log");
		
		dest.getParentFile().mkdirs();

		Writer pw = new PrintWriter(new FileOutputStream(dest));
		pw.write(msg);
		pw.flush();
				
		pw.close();
	}
	
	public void write(List<ResourceEntry> current) throws IOException{
		File c= new File(FV_CONSTANTS.CURRENT);
		String dir=(Calendar.getInstance().getTime().getMonth()+1) + "_" + Calendar.getInstance().getTime().getDate() + "_" + (Calendar.getInstance().getTime().getYear()+1900) ;
		
		File dest = new File("." + File.separator + dir+ File.separator + "results.csv");
		
		if(c.exists() && !c.isDirectory()){
			dest.getParentFile().mkdirs();
			Utils.MoveFile(c, dest, true);
		}else{
			c.getParentFile().mkdirs();
		}
		
		Writer pw = new PrintWriter(new FileOutputStream(c));
		
		for(ResourceEntry re: current){
			pw.write(re.toString());
			pw.write("\n");
			pw.flush();
		}
		
		pw.close();
	}
}
