package org.nrg.fv.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.nrg.fv.entry.DiffEntry;
import org.nrg.fv.entry.EntryComparator;
import org.nrg.fv.entry.ResourceEntry;

public class BuildSummary {
	private static final String CREATED = "C";
	public static final String MODIFIED = "M";
	public static final String MODIFIED_ABSENT = "MA";
	public static final String STILL_ABSENT = "A";
	final String DELETED = "D";
	
	public  List<List<DiffEntry>> compare(List<ResourceEntry> o1, List<ResourceEntry> c1){
		ArrayList<List<DiffEntry>> des=new ArrayList<List<DiffEntry>>();
		des.add(new ArrayList<DiffEntry>());
		des.add(new ArrayList<DiffEntry>());
		des.add(new ArrayList<DiffEntry>());
		des.add(new ArrayList<DiffEntry>());
		des.add(new ArrayList<DiffEntry>());
		
		EntryComparator ec = new EntryComparator();
		
		List<ResourceEntry> old = new ArrayList<ResourceEntry>();
		old.addAll(o1);
		Collections.sort(old, ec);

		List<ResourceEntry> current = new ArrayList<ResourceEntry>();
		current.addAll(c1);
		Collections.sort(current, ec);
		
		while(!old.isEmpty()){
			ResourceEntry _old = old.remove(0);
			int i=Collections.binarySearch(current, _old, ec);
			if(i<0){
				DiffEntry de = new DiffEntry();
				de.setExpt(_old.getEXPT());
				de.setProject(_old.getPROJECT());
				de.setLevel(_old.getLEVEL());
				de.setId(_old.getID());
				de.setLevel_id(_old.getLEVEL_ID());
				de.setMode(DELETED);
				de.oldCount=_old.COUNT;
				de.oldModTime=_old.LAST_MODIFIED;
				des.get(0).add(de);
			}else{
				ResourceEntry _new=current.remove(i);
				if(_old.COUNT==_new.COUNT && _old.LAST_MODIFIED==_new.LAST_MODIFIED){
					if(_old.COUNT==0){
						DiffEntry de = new DiffEntry();
						de.setExpt(_old.getEXPT());
						de.setProject(_old.getPROJECT());
						de.setLevel(_old.getLEVEL());
						de.setId(_old.getID());
						de.setLevel_id(_old.getLEVEL_ID());
						de.setMode(STILL_ABSENT);
						de.setLabel(_new.getLABEL());
						des.get(1).add(de);
					}
				}else{
					if(_new.COUNT==_old.COUNT){
						//FILE COUNT IS THE SAME
						DiffEntry de = new DiffEntry();
						de.setExpt(_old.getEXPT());
						de.setProject(_old.getPROJECT());
						de.setLevel(_old.getLEVEL());
						de.setId(_old.getID());
						de.setLevel_id(_old.getLEVEL_ID());
						de.setMode(MODIFIED);
						de.oldCount=_old.COUNT;
						de.oldModTime=_old.LAST_MODIFIED;
						de.newCount=_new.COUNT;
						de.newModTime=_new.LAST_MODIFIED;
						des.get(2).add(de);
					}else if(_new.COUNT==0 && _old.COUNT>0){
						DiffEntry de = new DiffEntry();
						de.setExpt(_old.getEXPT());
						de.setProject(_old.getPROJECT());
						de.setLevel(_old.getLEVEL());
						de.setId(_old.getID());
						de.setLevel_id(_old.getLEVEL_ID());
						de.setMode(MODIFIED_ABSENT);
						de.oldCount=_old.COUNT;
						de.oldModTime=_old.LAST_MODIFIED;
						de.newCount=_new.COUNT;
						de.newModTime=_new.LAST_MODIFIED;
						des.get(4).add(de);
					}else if(_old.COUNT==0 && _new.COUNT>0){
						DiffEntry de = new DiffEntry();
						de.setExpt(_old.getEXPT());
						de.setProject(_old.getPROJECT());
						de.setLevel(_old.getLEVEL());
						de.setId(_old.getID());
						de.setLevel_id(_old.getLEVEL_ID());
						de.setMode(CREATED);
						de.oldCount=_old.COUNT;
						de.oldModTime=_old.LAST_MODIFIED;
						de.newCount=_new.COUNT;
						de.newModTime=_new.LAST_MODIFIED;
						des.get(3).add(de);
					}else{
						DiffEntry de = new DiffEntry();
						de.setExpt(_old.getEXPT());
						de.setProject(_old.getPROJECT());
						de.setLevel(_old.getLEVEL());
						de.setId(_old.getID());
						de.setLevel_id(_old.getLEVEL_ID());
						de.setMode(MODIFIED);
						de.oldCount=_old.COUNT;
						de.oldModTime=_old.LAST_MODIFIED;
						de.newCount=_new.COUNT;
						de.newModTime=_new.LAST_MODIFIED;
						des.get(2).add(de);
					}
				}
			}
		}
		
		for(ResourceEntry re: current){
			DiffEntry de = new DiffEntry();
			de.setExpt(re.getEXPT());
			de.setProject(re.getPROJECT());
			de.setLevel(re.getLEVEL());
			de.setId(re.getID());
			de.setLevel_id(re.getLEVEL_ID());
			de.setMode(CREATED);
			de.newCount=re.COUNT;
			de.newModTime=re.LAST_MODIFIED;
			des.get(3).add(de);
		}
		
		return des;
	}
	
}
