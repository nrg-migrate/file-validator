package org.nrg.fv.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.nrg.fv.constants.FV_CONSTANTS;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.reader.XDATXMLReader;
import org.xml.sax.SAXException;

public class SessionRetriever {
	public XnatImagesessiondataBean retrieveSession(String id) throws MalformedURLException,IOException,SAXException,Exception{
		BufferedReader br = null;
		String thisLine = null;
		
		URL site= new URL(FV_CONSTANTS.HOST + "/REST/experiments/" + id);
		
		URLConnection uc = null;
		
		uc = site.openConnection();

		uc.setRequestProperty("Authorization", "Basic "
				+ com.Ostermiller.util.Base64.encode(FV_CONSTANTS.USER));
		
		br = new BufferedReader(new InputStreamReader(uc.getInputStream()));

		org.nrg.xdat.bean.reader.XDATXMLReader reader = new XDATXMLReader();
		BaseElement base=reader.parse(br);
		
		if(!(base instanceof XnatImagesessiondataBean)){
			throw new Exception("Invalid session type:" + base.getSchemaElementName());
		}
		
		return (XnatImagesessiondataBean) base;
	}
	
	public static void main(String[] args) throws Exception {
		SessionRetriever elr=new SessionRetriever();
		try {
			XnatImagesessiondataBean session=elr.retrieveSession("CNDA0489_MR_002");
			System.out.println(session.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
