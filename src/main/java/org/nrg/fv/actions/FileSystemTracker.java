package org.nrg.fv.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringUtils;
import org.nrg.fv.constants.FV_CONSTANTS;
import org.nrg.fv.entry.ResourceEntry;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.bean.CatEntryBean;
import org.nrg.xdat.bean.XnatAbstractresourceBean;
import org.nrg.xdat.bean.XnatImageassessordataBean;
import org.nrg.xdat.bean.XnatImagescandataBean;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatReconstructedimagedataBean;
import org.nrg.xdat.bean.XnatResourceBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;
import org.nrg.xdat.bean.XnatResourceseriesBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.reader.XDATXMLReader;
import org.xml.sax.SAXException;

public class FileSystemTracker {
	public static boolean TRACK_RESOURCES=true;
	public static boolean TRACK_SCANS=true;
	public static boolean TRACK_RECONS=true;
	public static boolean TRACK_ASSESS=true;
	
	public List<ResourceEntry> trackFiles(XnatImagesessiondataBean session){
		final FileHistory fh = new FileHistory();
		final List<ResourceEntry> files=new ArrayList<ResourceEntry>();
		
		if(session.getResources_resource().size()>0 && TRACK_RESOURCES){
			for(final XnatAbstractresourceBean res:session.getResources_resource()){
				final ResourceEntry re=getResourceSummary(res,fh);
				re.LEVEL="resource";
				re.LEVEL_ID=res.getLabel();
				files.add(re);
			}
		}

		if(TRACK_SCANS){
			for(final XnatImagescandataBean scan:session.getScans_scan()){
				if(scan.getQuality()==null || !scan.getQuality().equalsIgnoreCase("unusable")){
					if(scan.getFile().size()>0){
						for(final XnatAbstractresourceBean res:scan.getFile()){
							final ResourceEntry re=getResourceSummary(res,fh);
							re.LEVEL="scan";
							re.LEVEL_ID=scan.getId();
							files.add(re);			
						}
					}else{
						final ResourceEntry re=new ResourceEntry("ABSENT");
						re.LEVEL="scan";
						re.LEVEL_ID=scan.getId();
						files.add(re);
					}
				}
			}
		}

		if(TRACK_RECONS){
			for(final XnatReconstructedimagedataBean scan:session.getReconstructions_reconstructedimage()){
				if(scan.getOut_file().size()>0){
					for(final XnatAbstractresourceBean res:scan.getOut_file()){
						final ResourceEntry re=getResourceSummary(res,fh);
						re.LEVEL="recon";
						re.LEVEL_ID=scan.getId();
						files.add(re);			
					}
				}else{
					final ResourceEntry re=new ResourceEntry("ABSENT");
					re.LEVEL="recon";
					re.LEVEL_ID=scan.getId();
					files.add(re);
				}
			}
		}

		if(TRACK_ASSESS){
			for(final XnatImageassessordataBean scan:session.getAssessors_assessor()){
				if(scan.getOut_file().size()>0){
					for(final XnatAbstractresourceBean res:scan.getOut_file()){
						final ResourceEntry re=getResourceSummary(res,fh);
						re.LEVEL="assessor";
						re.LEVEL_ID=scan.getId();
						files.add(re);			
					}
				}
			}
		}
		
		return files;
	}
	
	public ResourceEntry getResourceSummary(XnatAbstractresourceBean res,FileHistory fv){
		final ResourceEntry re = new ResourceEntry(res.getLabel());
		long starttime=Calendar.getInstance().getTimeInMillis();
		if(res instanceof XnatResourcecatalogBean){
			String fullPath = ((XnatResourcecatalogBean)res).getUri();

            File f = new File(fullPath);
            if (!f.exists())
            {
                f = new File(fullPath + ".gz");
            }

            if (f.exists()){
                try {
                    InputStream fis = new FileInputStream(f);
                    if (f.getName().endsWith(".gz"))
                    {
                        fis = new GZIPInputStream(fis);
                    }
                    XDATXMLReader reader = new XDATXMLReader();
                    BaseElement base = reader.parse(fis);

                    String parentPath = f.getParent();

                    if (base instanceof CatCatalogBean){
                        for(CatEntryBean entry: ((CatCatalogBean)base).getEntries_entry()){
                            String entryPath = StringUtils.replace(AppendRootPath(parentPath,entry.getUri()),"\\","/");
                            File _f=getFileOnLocalFileSystem(entryPath,fv);
                            if(_f!=null){
//                                if(_f.exists()){
                                	re.addFile(_f);
                                
//                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                }
            }

            if(FV_CONSTANTS.DEBUG)System.out.print("... cat:"+(Calendar.getInstance().getTimeInMillis()-starttime) + "ms");
		}else if(res instanceof XnatResourceBean){
//			File f = new File(((XnatResourceBean)res).getUri());
//            if (!f.exists() && !((XnatResourceBean)res).getUri().endsWith(".gz"))
//            {
//                f = new java.io.File(((XnatResourceBean)res).getUri() + ".gz");
//            }

	    	File f = fv.retrieveFile(((XnatResourceBean)res).getUri());
	        
	        if(f!=null){
	        	re.addFile(f);
	        }
	        
	        if(FV_CONSTANTS.DEBUG)System.out.print("... res:"+(Calendar.getInstance().getTimeInMillis()-starttime) + "ms");
			
        }else if(res instanceof XnatResourceseriesBean){
			String fullPath = ((XnatResourceseriesBean)res).getPath();
            if (!fullPath.endsWith(File.separator))
            {
                fullPath += File.separator;
            }
            
            File dir = new File(fullPath);
            if (dir.exists())
            {
                String pattern = ((XnatResourceseriesBean)res).getPattern();
                if (pattern==null)
                {
                    for(File f:dir.listFiles()){
                        re.addFile(f);
                    }
                }else{
                    XNATFileFilter filter= new XNATFileFilter();
                    filter.setPattern(pattern);
                    for(File f:dir.listFiles(filter)){
                        re.addFile(f);
                    }
                }
            }
            if(FV_CONSTANTS.DEBUG)System.out.print("... ser:"+(Calendar.getInstance().getTimeInMillis()-starttime) + "ms");
		}
		
		return re;
	}
	

    protected File getFileOnLocalFileSystem(String fullPath,FileHistory fv) {
//        File f = new File(fullPath);
//        if (!f.exists()){
//            if (!fullPath.endsWith(".gz")){
//            	f= new File(fullPath + ".gz");
//            	if (!f.exists()){
//            		return null;
//            	}
//            }else{
//                return null;
//            }
//        }
    	File f = fv.retrieveFile(fullPath);
        
        return f;
    }
	
    public static String AppendRootPath(String root,String local)
    {
        if (root==null || root.equals(""))
        {
            return local;
        }else{
            if (IsAbsolutePath(local))
            {
                return local;
            }

            while (local.startsWith("\\") || local.startsWith("/"))
            {
                local = local.substring(1);
            }

            root=AppendSlash(root);

            return root + local;
        }
    }

    public static String AppendSlash(String root)
    {
        if (root==null || root.equals(""))
        {
            return null;
        }else{

            if (!root.endsWith("/") && !root.endsWith("\\")){
                root += File.separator;
            }

            return root;
        }
    }

	public static boolean IsAbsolutePath(String path)
	{
        if (path.startsWith("file:") || path.startsWith("http:") || path.startsWith("https:") || path.startsWith("srb:"))
        {
            return true;
        }
	    if (File.separator.equals("/"))
        {
            if (path.startsWith("/"))
            {
                return true;
            }
        }else{
            if (path.indexOf(":\\")!=-1)
            {
                return true;
            }else if (path.indexOf(":/")!=-1)
            {
                return true;
            }
        }

       return false;
	}
	


    public class XNATFileFilter implements java.io.FileFilter {
        private Pattern _pattern=null;
        public XNATFileFilter()
        {
        }

        public void setPattern(String pattern){
            _pattern=java.util.regex.Pattern.compile(pattern);
        }

        public boolean accept(File f)
        {
            if (_pattern.matcher(f.getName()).find())
            {
                return true;
            }else{
                return false;
            }

        }
    }

    public class XNATFileNameFilter implements FilenameFilter {
        private Pattern _pattern=null;
        public XNATFileNameFilter()
        {
        }

        public void setPattern(String pattern){
            _pattern=java.util.regex.Pattern.compile(pattern);
        }

        public boolean accept(File dir,String name)
        {
            if (_pattern.matcher(name).find())
            {
                return true;
            }else{
                return false;
            }
        }
    }
    
    public class FileHistory{
    	Map<File,File[]> history=new Hashtable<File,File[]>();
    	
    	public File retrieveFile(String path){
    		File f= new File(path);
    		File parent=f.getParentFile();
    		
    		if(!history.containsKey(parent)){
    			File[] children=parent.listFiles();
    			if(children==null){
    				return null;
    			}else{
    				history.put(parent,children);
    			}
    		}
    		
    		final String filename=f.getName();
    		final String filename_gz=f.getName()+".gz";
    		
    		File[] children=history.get(parent);
    		if(children!=null){
        		for(File child:children){
        			if(child.getName().equals(filename)||child.getName().equals(filename_gz)){
        				return child;
        			}
        		}
    		}
    		
    		return null;
    	}
    }
}
