package org.nrg.fv.entry;

public class DiffEntry {
	
	String id,project,expt,level,level_id,mode=null;
	public Integer oldCount,newCount=null;
	public Long oldModTime,newModTime=null;

	String label=null;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getExpt() {
		return expt;
	}

	public void setExpt(String expt) {
		this.expt = expt;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel_id() {
		return level_id;
	}

	public void setLevel_id(String levelId) {
		level_id = levelId;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
