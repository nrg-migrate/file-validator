package org.nrg.fv.entry;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.nrg.fv.utils.Utils;

import com.Ostermiller.util.CSVParser;

public class ResourceEntry implements Comparable{
	private static final char QUOTE = '\"';
	private String ID=null;
	public int COUNT=0;
	public long LAST_MODIFIED=0;
	public String PROJECT=null;
	public String EXPT=null;
	public String LABEL=null;

	public String LEVEL=null;
	public String LEVEL_ID=null;
	
	public ResourceEntry(){
		ID="NO LABEL";
	}
	
	public ResourceEntry(String i){
		if(i==null){
			ID="NO LABEL";
		}else{
			ID=StringUtils.remove(i, ',');
		}
	}
	
	public void addFile(File f){
		COUNT++;
		
//		long f_last=f.lastModified();
//		if(f_last>LAST_MODIFIED){
//			LAST_MODIFIED+=f.lastModified();
//		}
	}
	
	public static ResourceEntry parse(String line){
		ArrayList<String> list=Utils.CommaDelimitedStringToArrayList(line);
		
		ResourceEntry re = new ResourceEntry(list.get(0));
		re.COUNT=Integer.parseInt(list.get(1));
		re.LAST_MODIFIED=Long.parseLong(list.get(2));
		re.PROJECT=list.get(3);
		re.EXPT=list.get(4);
		re.LEVEL=list.get(5);
		re.LEVEL_ID=list.get(6);
		
		return re;
	}
	
	public String toString(){
		StringBuffer sb=new StringBuffer();
		sb.append(getID()).append(",");
		sb.append(this.COUNT).append(",");
		sb.append(this.LAST_MODIFIED).append(",");
		sb.append(this.PROJECT).append(",");
		sb.append(this.EXPT).append(",");
		sb.append(this.LEVEL).append(",");
		sb.append(this.LEVEL_ID);
		
		return sb.toString();
	}

	public String getID() {
		return (ID==null)?"NO LABEL":ID.toString();
	}

	public String getCOUNT() {
		return String.valueOf(COUNT);
	}

	public String getLastModified() {
		return String.valueOf(LAST_MODIFIED);
	}

	public String getPROJECT() {
		return (PROJECT==null)?"":PROJECT;
	}

	public String getEXPT() {
		return (EXPT==null)?"":EXPT;
	}

	public String getLEVEL() {
		return (LEVEL==null)?"":LEVEL;
	}

	public String getLEVEL_ID() {
		return (LEVEL_ID==null)?"":LEVEL_ID;
	}

	public String getLABEL() {
		return LABEL;
	}

	public void setLABEL(String lABEL) {
		LABEL = lABEL;
	}

	public int compareTo(Object v2) {
		ResourceEntry value2 = (ResourceEntry)v2;
		
		int _return = this.getPROJECT().compareTo(value2.getPROJECT());
        if(_return==0){
        	_return = this.getEXPT().compareTo(value2.getEXPT());
        	
        	if(_return==0){
        		_return = this.getLEVEL().compareTo(value2.getLEVEL());
            	
            	if(_return==0){
            		_return = this.getLEVEL_ID().compareTo(value2.getLEVEL_ID());
                	
                	if(_return==0){
                		return this.getID().compareTo(value2.getID());
                    }else{
                    	return _return;
                    }
                }else{
                	return _return;
                }
            }else{
            	return _return;
            }
        }else{
        	return _return;
        }
	}
	
	
}
