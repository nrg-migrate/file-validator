package org.nrg.fv.entry;

import java.util.Comparator;

public class EntryComparator extends Object implements Comparator<ResourceEntry> {

    public int compare(ResourceEntry value1, ResourceEntry value2) {        
        int _return = value1.getPROJECT().compareTo(value2.getPROJECT());
        if(_return==0){
        	_return = value1.getEXPT().compareTo(value2.getEXPT());
        	
        	if(_return==0){
        		_return = value1.getLEVEL().compareTo(value2.getLEVEL());
            	
            	if(_return==0){
            		_return = value1.getLEVEL_ID().compareTo(value2.getLEVEL_ID());
                	
                	if(_return==0){
                		_return = value1.getID().compareTo(value2.getID());
                        return _return;
                    }else{
                    	return _return;
                    }
                }else{
                	return _return;
                }
            }else{
            	return _return;
            }
        }else{
        	return _return;
        }
    }

}
