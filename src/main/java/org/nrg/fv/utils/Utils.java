package org.nrg.fv.utils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class Utils {


	/**
	 * Translates the comma-delimited string to an ArrayList of strings.
	 * @param s
	 * @return
	 */
	public static ArrayList<String> CommaDelimitedStringToArrayList(String s)
	{
		return DelimitedStringToArrayList(s,",");
	}

	/**
	 * Translates the comma-delimited string to an ArrayList of strings.
	 * @param s
	 * @return
	 */
	public static ArrayList<String> DelimitedStringToArrayList(String s, String delimiter)
	{
		if(s.trim().equals(""))return new ArrayList<String>();
		
		ArrayList<String> al = new ArrayList<String>();

		while(s.indexOf(delimiter) != -1)
		{
			al.add(s.substring(0,s.indexOf(delimiter)));
			s = s.substring(s.indexOf(delimiter) + 1);
		}

		al.add(s);

		return al;
	}

    /**
     * Translates the comma-delimited string to an ArrayList of strings.
     * @param s
     * @return
     */
    public static ArrayList<String> CommaDelimitedStringToArrayList(String s,boolean trim)
    {
        return DelimitedStringToArrayList(s,",",trim);
    }

    /**
     * Translates the comma-delimited string to an ArrayList of strings.
     * @param s
     * @return
     */
    public static ArrayList<String> DelimitedStringToArrayList(String s, String delimiter,boolean trim)
    {
        ArrayList<String> al = new ArrayList<String>();

        while(s.indexOf(delimiter) != -1)
        {
            al.add(s.substring(0,s.indexOf(delimiter)).trim());
            s = s.substring(s.indexOf(delimiter) + 1);
        }

        if (s.length() > 0)
        {
            if (trim){
                al.add(s.trim());
            }else{
                al.add(s);
            }
        }

        return al;
    }

	public static ArrayList FileLinesToArrayList(File f) throws FileNotFoundException, IOException{
	    ArrayList al = new ArrayList();

	    FileInputStream in = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(in);
        StringBuffer sb = new StringBuffer();
        while (dis.available() !=0)
		{
			al.add(dis.readLine());
		}

        dis.close();
	    return al;
	}

    public static ArrayList<ArrayList> CSVFileToArrayList(File f) throws FileNotFoundException, IOException{
        ArrayList<ArrayList> all = new ArrayList<ArrayList>();
        ArrayList rows = FileLinesToArrayList(f);
        Iterator rowIter = rows.iterator();
        while (rowIter.hasNext())
        {
            String row = (String)rowIter.next();
            all.add(Utils.CommaDelimitedStringToArrayList(row));
        }
        all.trimToSize();
        return all;
    }
	
	private static void MoveFile(File src, File dest,boolean overwrite,boolean deleteDIR)throws FileNotFoundException,IOException{
		boolean moved = false;

		if(!src.exists())throw new FileNotFoundException(src.getAbsolutePath()); 
		
            if (!dest.exists()){
                try {
                    moved = src.renameTo(dest);
                } catch (Throwable e) {
                }
            }
    
            if (!moved)
            {
                InputStream in = new FileInputStream(src);
    
                if (!dest.exists())
                {
                    dest.getParentFile().mkdirs();
                    dest.createNewFile();
                }else if (!overwrite){
                    return;
                }
                OutputStream out = new FileOutputStream(dest);
    
                // Transfer bytes from in to out
                byte[] buf = new byte[8 * 1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                try {
                    in.close();
                    out.close();
                } catch (IOException e) {
                }
    
                src.delete();
            }else{
                System.out.println("Rename successful: " + dest.getAbsolutePath());
            }
	}

	public static void MoveFile(File src, File dest,boolean overwrite) throws FileNotFoundException,IOException{
	    MoveFile(src,dest,overwrite,false);
	}

}
